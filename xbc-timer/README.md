### 插件介绍
- timer库支持获取当前日期时间、时间差、延迟执行、自定义多个轮询等45个方法

### 插件特色
- 无论项目多大，全局仅仅只需要一个定时器，减少内存消耗，防止定时器过多导致的系统崩溃

### 交流沟通
<p><font size="5" color="blue">加群交流QQ群：1078718987</font></p>

### 引入SDK
```javascript
import timer from './static/timer.js'
```
### 内置方法
#### 1、start(reset) 开启定时任务
reset 布尔值，是否开启单页任务，即进入新页面时，重置所有定时任务，默认true，即开启
```javascript
timer.start()
```
#### 2、remove(taskId) 移除指定定时任务
```javascript
// 移除指定单个任务
timer.remove('test')
// 移除指定多个任务
timer.remove(['test1', 'test2'])
```
#### 3、clear() 移除所有定时任务
```javascript
timer.clear()
```
#### 4、delay(tasks) 延时任务链(每个任务仅执行一次)
tasks 要延时执行的任务，支持单个(JSON)，也支持同时多个(jsonArray)，包含的参数有：  
	func-任务名（特别说明，不能加括号）  
	delay-延迟执行时间，单位毫秒  
	param-任务参数，支持合法的格式  
```javascript
// 执行单个延时任务
timer.delay({
	func: this.test,
	delay: 5000
})
// 执行多个延时任务
timer.delay([
	{	// 延迟5秒执行
		func: this.test1,
		delay: 5000
	},
	{	// 延迟10秒执行
		func: this.test2,
		delay: 10000
	}
])
```
#### 5、chain(tasks) 定时任务链(每个任务按照指定间隔循环执行)
tasks 要延时执行的任务，支持单个(JSON)，也支持同时多个(jsonArray)，包含的参数有：  
	id-任务ID    
	func-任务名（特别说明，不能加括号）  
	delay-延迟执行时间，单位毫秒  
	param-任务参数，支持合法的格式  
	interval-间隔执行时间，单位毫秒，默认1000  
	time-定时执行时间，支持yyyy-mm-dd hh:mm:ss、yyyy-mm-dd、hh:mm:ss
```javascript
// 同时添加多个循环任务
timer.chain([
	{
		id: 'test1',
		func: this.test1,
		interval: 1000,
		param: 5
	},
	{
		id: 'test2',
		func: this.test2,
		interval: 1000,
		param: {name: 'test'}
	}
])
// 同时添加多个定时任务
timer.chain([
	{	// 在指定的某个具体时间定时执行（年月日时分秒均须固定2位）
		id: 'test1',
		func: this.test1,
		param: 5,
		time: '2021-05-22 12:00:00'
	},
	{	// 在指定的某个日期的00:00:00定时执行（不传入时间时，默认00:00:00）
		id: 'test2',
		func: this.test2,
		time: '2021-05-22'
	},
	{	// 在每天的某个时间点定时执行（时分秒均须固定2位）
		id: 'test3',
		func: this.test3,
		time: '00:30:20'
	}
])
```
#### 6、getYearDates(year=0, bool=true, separ='-') 获取指定年月内的日期
year 整数值，年份(4位)，当前年份时传入0  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getYearDates()
```
#### 7、getYearMonthDates(year, month, isAll=false, bool=true, separ='-') 获取指定年月内的日期
year 整数值，年份(4位)  
month 整数值，月份  
isAll 布尔值，是否返回指定年月内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getYearMonthDates(2021, 11)
```
#### 8、getThisMonthDates(isAll=false, bool=true, separ='-') 获取本月内的日期
isAll 布尔值，是否返回本月内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getThisMonthDates()
```
#### 9、getLastMonthDates(isAll=false, bool=true, separ='-') 获取上月内的日期
isAll 布尔值，是否返回上月内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日    
```javascript
timer.getLastMonthDates()
```
#### 10、getNextMonthDates(isAll=false, bool=true, separ='-') 获取下月内的日期
isAll 布尔值，是否返回下月内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日    
```javascript
timer.getNextMonthDates()
```
#### 11、getRangeWeekDates(step=0, isAll=false, bool=true, separ='-') 获取向前向后推的X周内的日期
step 整数值，表当向前向后推的多少周，本周时为0  
isAll 布尔值，是否返回本周内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日    
```javascript
timer.getRangeWeekDates()
```
#### 12、getThisWeekDates(isAll=false, bool=true, separ='-') 获取本周日期
isAll 布尔值，是否返回本周内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getThisWeekDates()
```
#### 13、getLastWeekDates(isAll=false, bool=true, separ='-') 获取上周日期
isAll 布尔值，是否返回上周内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getLastWeekDates()
```
#### 14、getNextWeekDates(isAll=false, bool=true, separ='-') 获取下周日期
isAll 布尔值，是否返回下周内的所有连续日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getNextWeekDates()
```
#### 15、getTomorrow(bool=true, separ='-') 获取明天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getTomorrow()
```
#### 16、getYesterday(bool=true, separ='-') 获取昨天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getYesterday()
```
#### 17、getToday(bool=true, separ='-') 获取今天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getToday()
```
#### 18、getYearMonthLastDate(year=0, month=0, bool=true, separ='-') 获取指定年月的最后一天的日期
year 整数值，年份(4位)，当前年份时传入0  
month 整数值，月份，当前月份时传入0  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getYearMonthLastDate()
```
#### 19、getThisMonthLastDate(bool=true, separ='-') 获取本月最后一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getThisMonthLastDate()
```
#### 21、getLastMonthLastDate(bool=true, separ='-') 获取上月最后一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getLastMonthLastDate()
```
#### 21、getNextMonthLastDate(bool=true, separ='-') 获取下月最后一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getNextMonthLastDate()
```
#### 22、getRangeWeekLastDate(step=0, bool=true, separ='-') 获取向前向后推的X周最后一天的日期
step INT，获取向前向后的X周，0-本周，1-下周，2-后2周，-1-上周，-2-上2周  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getRangeWeekLastDate()
```
#### 23、getThisWeekLastDate(bool=true, separ='-') 获取本周最后一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getThisWeekLastDate()
```
#### 24、getLastWeekLastDate(bool=true, separ='-') 获取上周最后一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getLastWeekLastDate()
```
#### 25、getNextWeekLastDate(bool=true, separ='-') 获取下周最后一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getNextWeekLastDate()
```
#### 26、getRangeWeekFirstDate(step=0, bool=true, separ='-') 获取向前向后推的X周第一天的日期
step INT，获取向前向后的X周，0-本周，1-下周，2-后2周，-1-上周，-2-上2周  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getRangeWeekFirstDate()
```
#### 27、getThisWeekFirstDate(bool=true, separ='-') 获取本周第一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getThisWeekFirstDate()
```
#### 28、getLastWeekFirstDate(bool=true, separ='-') 获取上周第一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getLastWeekFirstDate()
```
#### 29、getNextWeekFirstDate(bool=true, separ='-') 获取下周第一天的日期
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getNextWeekFirstDate()
```
#### 30、getLatestDates(days=7, isAll=false, bool=true, separ='-') 获取最近几天内的日期
days 整数，最近几天，默认7天  
isAll 布尔值，是否显示所有日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getLatestDates()
```
#### 31、getNextDates(days=7, isAll=false, bool=true, separ='-') 获取未来几天内的日期
days 整数，未来几天，默认7天  
isAll 布尔值，是否显示所有日期，默认否  
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 字符串，年月日分隔，默认-，设置为text时为年月日
```javascript
timer.getNextDates()
```
#### 32、getYear(bool) 获取当前年份
bool 布尔值，是否获取完整年份，true-是（默认），false-否
```javascript
timer.getYear()
```
#### 33、getMonth(bool) 获取当前月份
bool 布尔值，是否固定月份为2位，true-是（默认），false-否
```javascript
timer.getMonth()
```
#### 34、getDate(bool) 获取当前日期
bool 布尔值，是否固定日期为2位，true-是（默认），false-否
```javascript
timer.getDate()
```
#### 35、getYearMonth(bool, separ) 获取当前年月
bool 布尔值，是否设置月份固定2位，true-是（默认），false-否  
separ 年月之间的分隔，默认-，设置为text时为年月日
```javascript
timer.getYearMonth()
```
#### 36、getYearMonthDate(bool, separ) 获取当前年月日
bool 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ 年月日分隔，默认-，设置为text时为年月日  
```javascript
timer.getYearMonthDate()
```
#### 37、getDateTime(bool1, separ1, bool2, separ2) 获取当前日期时间
bool1 布尔值，是否设置月日固定2位，true-是（默认），false-否  
separ1 年月日分隔，默认-  ，设置为text时为年月日
bool2 布尔值，是否设置时分秒固定2位，true-是（默认），false-否  
separ3 时分秒之间的分隔，默认:，设置为text时为时分秒  
```javascript
timer.getDateTime()
```
#### 38、getTime(bool, separ) 获取当前时分秒
bool 布尔值，是否设置时分秒固定2位，true-是（默认），false-否  
separ 时分秒之间的分隔，默认:，设置为text时为时分秒  
```javascript
timer.getTime()
```
#### 39、getHours(bool) 获取当前小时数
bool 布尔值，是否固定小时数为2位，true-是（默认），false-否
```javascript
timer.getHours()
```
#### 40、getMinutes(bool) 获取当前分钟数
bool 布尔值，是否固定分钟数为2位，true-是（默认），false-否
```javascript
timer.getMinutes()
```
#### 41、getSeconds(bool) 获取当前秒数
bool 布尔值，是否固定秒数为2位，true-是（默认），false-否
```javascript
timer.getSeconds()
```
#### 42、getStamp(bool) 获取当前时间戳，单位为毫秒
bool 布尔值，设置单位是否为毫秒，默认是，false时返回的时间戳单位为秒
```javascript
timer.getStamp()
```
#### 43、getWeek(type) 获取当前星期几
type 返回的星期格式，可选值有：  
	0-周x，默认  
	1-星期x  
	2-礼拜x  
	3-英文  
	4-英文简写  

```javascript
timer.getWeek()
```
#### 44、getDiff(timestamp) 获取某个时间距离当前时间多久
timestamp 时间戳，支持秒和毫秒  
return 刚刚、x分钟前、x小时前、x天前、年-月-日  
```javascript
timer.getDiff('1621700428560')
```
#### 45、roll(param) 数字自增长
param.number: 增加到的目标值，默认为0  
param.step: 增加的步长值，默认为1  
param.start: 初始值，默认为0  
param.speed: 变化的时间，单位毫秒，默认10毫秒  
param.formate: 是否开启数值千分位，默认为false  
param.change: 监听数值增值变化  
```javascript
timer.roll({
	number: 100,
	step: 1,
	change: number=>{
		console.log(number)
	}
});
```
#### 46、countdown(param) 倒计时
param.id: 唯一标识符  
param.seconds: 倒计时时间，单位秒  
param.change: 倒计时回调  
param.done: 倒计时结束回调
```javascript
timer.countdown({
	id: 'test',
	seconds: 36,
	change: res=>{
		this.time = `${res.days}天${res.hours}时${res.minutes}分${res.seconds}秒`
	},
	done: ()=>{
		console.log('倒计时结束')
	}
});
```
支持的方法有：pause()、start(seconds)、restart(seconds)  
1) pause() 暂停当前倒计时
```javascript
oTimer.pause();
```
2) start(seconds) 开始当前倒计时  
seconds: 可选，单位为秒，有值时，会从当前值开始倒计时
```javascript
// 从当前暂停时间开始继续倒计时
oTimer.start();

// 从200秒开始倒计时
oTimer.start(200);
```
3) restart(seconds) 重新开始倒计时  
seconds: 可选，单位为秒，有值时，会从当前值重新开始倒计时
```javascript
// 从默认开始时间重新倒计时
oTimer.restart();

// 从200秒重新倒计时
oTimer.restart(200);
```