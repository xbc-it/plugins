### Introduction
- Support to obtain the current date and time, time difference, delay execution, customize multiple rotation training and other functions.

### Features
- No matter how big the project is, only one timer is needed to reduce memory consumption and prevent application crash caused by too many timers.

### Import
```javascript
import timer from 'xbc-timer'
```
### Methods
#### 1、start(reset): start timing task
reset Boolean，Open single page task or not, that is, reset all timing tasks when entering a new page. The default is true, that is, open
```javascript
timer.start()
```
#### 2、remove(taskId): remove the task
```javascript
// remove the task
timer.remove('test')
// remove multiple tasks
timer.remove(['test1', 'test2'])
```
#### 3、clear(): remove all tasks
```javascript
timer.clear()
```
#### 4、delay(tasks): delayed task chain (each task is executed only once)
tasks: To delay the execution of tasks, it supports single (JSON) and multiple (jsonArray) at the same time. The parameters include：  
	func-Task name (special instructions, no parentheses)  
	delay-Latency in milliseconds  
	param-Task parameter, support legal format  
```javascript
// exec a single delayed task
timer.delay({
	func: this.test,
	delay: 5000
})
// exec multiple delayed task
timer.delay([
	{	// Delay execution by 5 seconds
		func: this.test1,
		delay: 5000
	},
	{	// Delay execution by 10 seconds
		func: this.test2,
		delay: 10000
	}
])
```
#### 5、chain(tasks): timed task chain (each task circulates at specified intervals)
tasks: To circular the execution of tasks, it supports single (JSON) and multiple (jsonArray) at the same time. The parameters include：    
	id-TaskId  
	func-Task name (special instructions, no parentheses)  
	delay-Latency in milliseconds  
	param-Task parameter, support legal format  
	interval-Interval execution time, in milliseconds, default 1000  
	time-Timing execution time, support: yyyy-mm-dd hh:mm:ss、yyyy-mm-dd、hh:mm:ss  
```javascript
// Add multiple loop tasks at the same time
timer.chain([
	{
		id: 'test1',
		func: this.test1,
		delay: 2000,
		interval: 1000,
		param: 5
	},
	{
		id: 'test2',
		func: this.test2,
		interval: 1000,
		param: {name: 'test'}
	}
])
// Add multiple timed tasks at the same time
timer.chain([
	{	// Executed at a specified time (2 digits must be fixed for month, year, day, hour, minute and second)
		id: 'test1',
		func: this.test1,
		param: 5,
		time: '2021-05-22 12:00:00'
	},
	{	// Execute at the specified time of 00:00:00 on a specified date (when no time is passed in, 00:00:00 is the default)
		id: 'test2',
		func: this.test2,
		time: '2021-05-22'
	},
	{	// At a certain time point of every day (2 bits of time, minute and second must be fixed)
		id: 'test3',
		func: this.test3,
		time: '00:30:20'
	}
])
```
#### 6、getYear(bool): get the current year
bool: Boolean, whether to get the complete year, true Yes (default), false no
```javascript
timer.getYear()
```
#### 7、getMonth(bool): get the current month
bool: Boolean, whether to get the complete month, true Yes (default), false no
```javascript
timer.getMonth()
```
#### 8、getDate(bool): get the current date
bool: Boolean，whether the fixed date is 2 digits, true Yes (default), false no
```javascript
timer.getDate()
```
#### 9、getYearMonth(bool, separ): get the current year-month
bool: Boolean，whether the fixed month is 2 digits, true Yes (default), false no  
separ: The separation between year and year, default-
```javascript
timer.getYearMonth()
```
#### 10、getYearMonthDate(bool, separ): get the current year-month-date
bool: Boolean，whether the fixed month and date is 2 digits, true Yes (default), false no  
separ: The separation between year and month, default-
```javascript
timer.getYearMonthDate()
```
#### 11、getDateTime(bool1, separ1, bool2, separ2): get the current datetime
bool1: Boolean，whether the fixed month and date is 2 digits, true Yes (default), false no  
separ1: The separation between year month and date, default-  
bool2: Boolean，whether the fixed houres minutes and seconds is 2 digits, true Yes (default), false no  
separ3: The separation between houres minutes and seconds, default:
```javascript
timer.getDateTime()
```
#### 12、getTime(bool, separ): get the current time
bool: Boolean，whether the fixed houres minutes and seconds is 2 digits, true Yes (default), false no  
separ: The separation between houres minutes and seconds, default:
```javascript
timer.getTime()
```
#### 13、getHours(bool): get the current hours
bool: Boolean，whether the fixed hours is 2 digits, true Yes (default), false no
```javascript
timer.getHours()
```
#### 14、getMinutes(bool): get the current minutes
bool: Boolean，whether the fixed minutes is 2 digits, true Yes (default), false no
```javascript
timer.getMinutes()
```
#### 15、getSeconds(bool): get the current seconds
bool: Boolean，whether the fixed seconds is 2 digits, true Yes (default), false no
```javascript
timer.getSeconds()
```
#### 16、getStamp(bool): get the current timestamp
bool: Boolean，set whether the unit is Ms. by default, the unit of time stamp returned when false is seconds
```javascript
timer.getStamp()
```
#### 17、getWeek(type): get the current week
type: Return the week format, optional values are：  
	0-周x，default  
	1-星期x  
	2-礼拜x  
	3-English  
	4-Short for English  

```javascript
timer.getWeek()
```
#### 18、getDiff(timestamp): get the distance between a certain time and the current time
timestamp: timestamp，seconds or milliseconds  
return: 刚刚、x分钟前、x小时前、x天前、年-月-日  
```javascript
timer.getDiff('1621700428560')
```
#### 19、roll(param) roll number by self
param.number: target number, default 0  
param.step: roll step number，default 1  
param.start: start number，default 0  
param.speed: roll speed，millisecond，default 10  
param.formate: localeString，default false  
param.change: listen roll number  
```javascript
timer.roll({
	number: 100,
	step: 1,
	change: number=>{
		console.log(number)
	}
});
```
#### 20、countdown(param) count down
param.id: unique identifier  
param.seconds: seconds  
param.change: change callback, function  
param.done: end callback, function
return: return the currently instantiated countdown object
```javascript
let oTimer = timer.countdown({
	id: 'test',
	seconds: 36,
	change: res=>{
		this.time = `${res.days}天${res.hours}时${res.minutes}分${res.seconds}秒`
	},
	done: ()=>{
		console.log('倒计时结束')
	}
});
```
contain methods：pause()、start(seconds)、restart(seconds)
1) pause() pause current countdown
```javascript
oTimer.pause();
```
2) start(seconds) start current countdown  
seconds: optional, in seconds. If there is a value, the countdown will start from the current value
```javascript
// the countdown will start from the pause value
oTimer.start();

// the countdown will start from the current value
oTimer.start(200);
```
3) restart(seconds) restart current countdown  
seconds: optional, in seconds. If there is a value, the countdown will restart from the current value
```javascript
// the countdown will restart from the pause value
oTimer.restart();

// the countdown will restart from the current value
oTimer.restart(200);
```