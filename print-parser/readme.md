## 云打印方法

### 安装

云打印为一个 js 文件，使用前请先安装 jquery 和 axios 依赖

```
// 引入JS
import Parser from './print-parse'
```

### 1.init(data, options) 初始化

data: 要打印的数据，包含 temp 和 sources 两部分  
options  
· render: 可选，是否自动渲染内容到页面，默认 true  
· renderType: 可选，渲染模式，可选项有 local(本地, 默认)、cloud(云)  
· ajax: 可选，是否自动上传 HTML 到服务端，默认 false  
· ajaxUrl: 上传 HTML 到服务端的完整 API 地址，ajax 为 true 时必填  
· ajaxSuccess：上传 HTML 成功后的回调，ajax 为 true 时触发  
· ajaxError：上传 HTML 失败后的回调，ajax 为 true 时触发

```
Parser.init(data, {
	render: true,   // 是否自动渲染并输出，APP需要自动渲染，PC根据业务场景设置
})
```

### 2.getHtml(type, doc) 获取 HTML 字符串

type: 可选，打印类型 local-本地，cloud-云(默认)  
doc: 可选，是否需要输出完成 HTML 结构(包含 html/head/body 等)，可选项 true(是， 默认)，false(否)

```
let localHtml = Parser.getHtml('local')
let cloudHtml = Parser.getHtml('cloud')
```

### 3.getData(params) 获取打印模板数据

params  
· billCat: 必选，单据类型  
· billId: 必选，单据 ID  
· tempId: 必选，模板 ID  
· printPath: 必选，接口打印路径  
· token: 必选，accessToken

```
let { data } = await Parser.getData(params)
```

### 4.postData() 上传数据到服务器

```
let res = await Parser.postData()
```

### 5.preview() 预览

```
Parser.preview()
```

### 6.destroy() 销毁整个对象

```
Parser.destroy()
```

特别说明：页面卸载时一定要手动调用
